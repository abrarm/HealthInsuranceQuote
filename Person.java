class Person{
	
	
	public Person() {
		this.healthInsurancePremium = 5000;
		this.currentHealth = new CurrentHealth();
		this.habits = new Habits();
	}
	
	String name;
	String lname;
	String fname;
	
	void setName(String name) {
		
		String[] str = name.split(" ");
		this.lname = str[1];
		this.fname = str[0];
	}
	
	String gender;
	int age;	
	
	long healthInsurancePremium;
	 
	CurrentHealth currentHealth;
	Habits habits;
	
}

class CurrentHealth{
	
	String hypertension;
	String bloodPressure;
	String bloodSugar;
	String overweight;
}

class Habits{
	
	String smoking;
	String alcohol;
	String dailyExercise;
	String drugs;
	
}