import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InsuranceQuote {

	public static void main(String[] args) {

		Person person = new Person();
		
		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.print("Name: ");
			person.name = br.readLine();
			
			System.out.print("\nGender: ");
			person.gender = br.readLine();
			
			System.out.print("\nAge: ");
			String ageString = br.readLine();
			while(! ageString.matches("\\d+")) {
			
				System.out.print("\nEnter age in number!");
				System.out.print("\nAge: ");
				ageString = br.readLine();
				
			}
			person.age = Integer.parseInt(ageString);
			
			System.out.println("\nCurrent health: ");
			System.out.print("\t Hypertension: ");
			person.currentHealth.hypertension = br.readLine();
			System.out.print("\n\t Blood pressure: ");
			person.currentHealth.bloodPressure = br.readLine();
			System.out.print("\n\t Blood sugar: ");
			person.currentHealth.bloodSugar = br.readLine();
			System.out.print("\n\t Overweight: ");
			person.currentHealth.overweight = br.readLine();
			
			System.out.print("\nHabits: ");
			System.out.print("\n\t Smoking: ");
			person.habits.smoking = br.readLine();
			System.out.print("\n\t Alcohol: ");
			person.habits.alcohol = br.readLine();
			System.out.print("\n\t Daily exercise: ");
			person.habits.dailyExercise = br.readLine();
			System.out.print("\n\t Drugs: ");
			person.habits.drugs = br.readLine();
			
			//Calculation
			if(person.age >= 18 && person.age <25)
				person.healthInsurancePremium = (long) (person.healthInsurancePremium * 1.1);
			if(person.age >= 25 && person.age <30)
				person.healthInsurancePremium = (long) (person.healthInsurancePremium * 1.2);
			if(person.age >= 30 && person.age <35)
				person.healthInsurancePremium = (long) (person.healthInsurancePremium * 1.3);
			if(person.age >= 35 && person.age <40)
				person.healthInsurancePremium = (long) (person.healthInsurancePremium * 1.4);
			 if(person.age >= 40 && person.age <45)
				person.healthInsurancePremium = (long) (person.healthInsurancePremium * 1.6);
			 if(person.age >= 40 && person.age <45)
					person.healthInsurancePremium = (long) (person.healthInsurancePremium * 1.6);
			 if(person.age >= 45 && person.age <50)
					person.healthInsurancePremium = (long) (person.healthInsurancePremium * 1.8);
			 if(person.age >= 50 && person.age <55)
					person.healthInsurancePremium = (long) (person.healthInsurancePremium * 2);
			 if(person.age >= 55 && person.age <60)
					person.healthInsurancePremium = (long) (person.healthInsurancePremium * 2.2);
			 if(person.age >= 60 && person.age <65)
					person.healthInsurancePremium = (long) (person.healthInsurancePremium * 2.4);
			 
					
			 long temp = person.healthInsurancePremium ;
			if(person.gender.equalsIgnoreCase("Male"))
				person.healthInsurancePremium = (long) (temp * 1.02);
			//HealthCond
			if(person.currentHealth.overweight.equalsIgnoreCase("Yes"))
				person.healthInsurancePremium = (long) (temp * 1.01);
			
			if(person.currentHealth.hypertension.equalsIgnoreCase("Yes"))
				person.healthInsurancePremium = (long) (temp * 1.01);
			
			if(person.currentHealth.bloodSugar.equalsIgnoreCase("Yes"))
				person.healthInsurancePremium = (long) (temp * 1.01);
			
			if(person.currentHealth.bloodPressure.equalsIgnoreCase("Yes"))
				person.healthInsurancePremium = (long) (temp * 1.01);
			
			
			
			//Habits
	
			
			if(person.habits.dailyExercise.equalsIgnoreCase("Yes"))
				person.healthInsurancePremium = (long) (temp  - (temp * 0.03));
			else
				person.healthInsurancePremium = (long) (temp * 1.03);
			
			if(person.habits.alcohol.equalsIgnoreCase("Yes"))
				person.healthInsurancePremium = (long) (temp * 1.03);
			else
				person.healthInsurancePremium = (long) (temp  - (temp * 0.03));
			
			
			if(person.habits.drugs.equalsIgnoreCase("Yes"))
				person.healthInsurancePremium = (long) (person.healthInsurancePremium * 1.03);
			else
				person.healthInsurancePremium = (long) (temp  - (temp * 0.03));
			
			if(person.habits.smoking.equalsIgnoreCase("Yes"))
				person.healthInsurancePremium = (long) (temp * 1.03);
			else
				person.healthInsurancePremium = (long) (temp  - (temp * 0.03));
			
			
			
			System.out.println();
			System.out.print("Health Insurance Premium for ");
			if(person.gender.equalsIgnoreCase("Male"))
				System.out.print("Mr. ");
			else if(person.gender.equalsIgnoreCase("female"))
				System.out.print("Ms. ");
			else
				System.out.print(" ");
			System.out.print(person.lname + ":Rs. " + person.healthInsurancePremium);
			
			
			
			
			
		}catch(IOException e) {
			 e.printStackTrace();			
		}finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
		
	}

}
